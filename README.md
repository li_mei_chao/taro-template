# taro template



## script

1.  **安装依赖** 

  `yarn` 等

2. **解决报错**
  删除 `nodemodules/@types/hoist-non-react-statics`
3. **本地开发预览**

  `yarn dev:weapp`

4. **git 代码提交**

  `yarn commit`

  按照 yarn commit 提示填写(除第一条外都是非必填项)

  `git push`

## 项目集成

  1. taro-axios (等同于axios)
  2. vant [文档](https://antm-js.gitee.io/vantui/)
  3. scss
  4. linaria(等同于 styled-components, css in js 的一种, CSS Modules 默认未开启)
  5. redux
  6. dayjs [文档](https://dayjs.fenxianglu.cn/category/)

## 目录结构

|-- config **taro 配置**
    |-- dev.js
    |-- index.js
    |-- prod.js
|-- src
    |-- app.config.ts
    |-- app.scss **全局css**
    |-- app.tsx
    |-- index.html
    |-- api **接口api**
    |   |-- demo.ts
    |   |-- type.ts
    |-- assets **静态资源**
    |   |-- icon
    |   |   |-- home-open.png
    |   |   |-- home.png
    |   |   |-- my-open.png
    |   |   |-- my.png
    |   |-- image
    |-- components **全局组件**
    |-- hooks **全局hooks**
    |-- pages **页面**
    |   |-- index
    |   |   |-- index.config.ts
    |   |   |-- index.tsx
    |   |   |-- style.ts
    |   |-- my
    |   |   |-- my.config.ts
    |   |   |-- my.tsx
    |   |   |-- style.ts
    |   |-- template
    |       |-- style.ts
    |       |-- tem.config.ts
    |       |-- template.tsx
    |-- store **redux**
    |   |-- index.ts
    |   |-- reducer.ts
    |   |-- type.ts
    |   |-- user
    |       |-- action.ts
    |       |-- constants.ts
    |       |-- reducer.ts
    |       |-- types.ts
    |-- utils **工具**
        |-- config.ts **项目配置**
        |-- eventbus.ts 
        |-- request.ts **axios实例**
