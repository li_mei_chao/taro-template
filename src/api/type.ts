import { IUserinfo } from "@/store/user/types";

export interface IResult {
  code: string;
  /**说明 */
  message: string;
}
export interface IGetUserInfoResult extends IResult, IUserinfo {}
