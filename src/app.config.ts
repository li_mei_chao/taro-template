export default defineAppConfig({
  pages: ["pages/index/index", "pages/my/my"],
  window: {
    backgroundTextStyle: "light",
    navigationBarBackgroundColor: "#fff",
    navigationBarTitleText: "Mrose",
    navigationBarTextStyle: "black",
  },
  tabBar: {
    color: "#101010",
    selectedColor: "#4567ee",
    borderStyle: "white",
    backgroundColor: "#ffffff",
    list: [
      {
        pagePath: "pages/index/index",
        iconPath: "assets/icon/home.png",
        selectedIconPath: "assets/icon/home-open.png",
        text: "首页",
      },
      {
        pagePath: "pages/my/my",
        iconPath: "assets/icon/my.png",
        selectedIconPath: "assets/icon/my-open.png",
        text: "我的",
      },
    ],
  },
});
