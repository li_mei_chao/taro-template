import { memo } from "react";
import { Button } from "@antmjs/vantui";
import { View } from "@tarojs/components";

import style from "./style.module.less";

console.log(style);

export default memo(function Index() {
  return (
    <View className={style.index}>
      <View className={style.a}>首页 这里使用的是CSSModule</View>
      <Button type="default">默认按钮</Button>
      <Button type="primary">主要按钮</Button>
      <Button type="info">信息按钮</Button>
      <Button type="warning">警告按钮</Button>
      <Button type="danger">危险按钮</Button>
    </View>
  );
});
