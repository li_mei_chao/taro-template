import { memo } from "react";

import { View } from "@tarojs/components";

import { Wrap } from "./style";

export default memo(function My() {
  return (
    <Wrap>
      <View>我的 这里使用的是css in js</View>
    </Wrap>
  );
});
