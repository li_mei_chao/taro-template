import React, { memo } from "react";

import { View } from "@tarojs/components";

import { Wrap } from "./style";

export default memo(function Demo() {
  return (
    <Wrap>
      <View>模板</View>
    </Wrap>
  );
});
