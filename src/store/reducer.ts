import { combineReducers } from "redux-immutable";
import { Record } from "immutable";

import user from "./user/reducer";

const cReducer = combineReducers({
  user,
});

export default cReducer;

export type AppState = Record<ReturnType<typeof cReducer>>;
