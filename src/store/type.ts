import { Map } from "immutable";

export interface StateMap<T> extends Map<string, any> {
  get<K extends keyof T>(key: K): T[K];
}
