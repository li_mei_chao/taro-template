import { Dispatch } from "redux";

import { actionType } from "./constants";
import type * as types from "./types";

export const setUserInfoAction = (userinfo: types.IUserinfo) => {
  return (dispatch: Dispatch) => {
    dispatch({
      type: actionType.SET_USERINFO,
      userinfo,
    });
  };
};
