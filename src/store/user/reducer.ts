import { Map } from "immutable";

import { actionType } from "./constants";

import { IUserinfo, IUserInfoAction } from "./types";

const defaultState = Map<keyof IUserinfo, string>({});

function reducer(state = defaultState, action: IUserInfoAction) {
  switch (action.type) {
    case actionType.SET_USERINFO:
      return (state = Map<keyof IUserinfo, any>(action.userinfo));
    default:
      return state;
  }
}

export default reducer;
