import { Action } from "redux";
import type { StateMap } from "../type";

export interface IUserinfo {
  /**用户名称 */
  name?: string;
}
export type IUserState = StateMap<IUserinfo>;
export interface IUserInfoAction extends Action {
  userinfo: IUserinfo;
}
