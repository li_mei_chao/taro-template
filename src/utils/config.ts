// const devBaseURL = 'http://192.168.20.144:8012/mrose/v1.0'
const devBaseURL = "https://demo.yilaiyiwang.com/mrose/v1.0";
// const proBaseURL = "http://192.168.20.136:8012/sicu/v1.0"
const proBaseURL = "https://demo.yilaiyiwang.com/mrose/v1.0";

export const BASE_URL =
  process.env.NODE_ENV === "development" ? devBaseURL : proBaseURL;

export const TIMEOUT = 5000;

export const defaultAvatar = require("@/assets/image/default-avarat.png");
