import { axios } from "taro-axios";
import type { AxiosRequestConfig } from "taro-axios";
import Taro from "@tarojs/taro";
import { BASE_URL, TIMEOUT } from "./config";

const instance = axios.create({
  baseURL: BASE_URL,
  timeout: TIMEOUT,
});

instance.interceptors.request.use(
  (config) => {
    // 1.发送网络请求时, 在界面的中间位置显示Loading的组件

    // 2.某一些请求要求用户必须携带token, 如果没有携带, 那么直接跳转到登录页面

    // 加token
    const token: string = Taro.getStorageSync("TOKEN");
    if (token) {
      config.headers.token = token;
    }
    return config;
  },
  (err) => {
    console.log("request error:", err);
  }
);

instance.interceptors.response.use(
  // 响应成功处理方式
  (res) => {
    return res;
  },
  // 响应失败处理方式
  (err) => {
    Taro.showToast({
      title: "网络错误",
      icon: "none",
    });
    return Promise.reject(err.message);
  }
);

export function request<T>(config: AxiosRequestConfig): Promise<T> {
  return new Promise((resolve, reject) => {
    instance(config)
      .then((res) => {
        resolve(res.data);
      })
      .catch((err) => {
        reject(err);
      });
  });
}
